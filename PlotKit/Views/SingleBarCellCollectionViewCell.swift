//
//  SingleBarCellCollectionViewCell.swift
//  PlotKit
//
//  Created by Jakub Mazur on 20/08/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import UIKit

class SingleBarCellCollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "SingleBarCellCollectionViewCell"

    private var visibleBarView: UIView?
    private var heightConstraint: NSLayoutConstraint?
    var offset: CGFloat = 0 {
        didSet {
            self.prepareVisibleView()
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.prepareVisibleView()
        self.layoutSubviews()
    }

    private func prepareVisibleView() {
        if self.visibleBarView == nil { self.createVisibleView() }
        heightConstraint?.isActive = false
        heightConstraint = visibleBarView?.heightAnchor.constraint(equalToConstant: offset)
        heightConstraint?.isActive = true
    }

    private func createVisibleView() {
        visibleBarView = UIView()
        visibleBarView?.backgroundColor = UIColor.orange
        self.addSubview(visibleBarView!)
        visibleBarView?.translatesAutoresizingMaskIntoConstraints = false
        visibleBarView?.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        visibleBarView?.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        visibleBarView?.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }

}
