//
//  BarGraphView.swift
//  PlotKit
//
//  Created by Jakub Mazur on 22/07/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import UIKit

@IBDesignable
public class BarGraphView: UIView {

    private var placeholderImageView: UIImageView?
    var scaleView: ScaleView!
    var barCollectionView: UICollectionView?
    internal var plotter: Plotter?
    var singleBarHeight: CGFloat = 0

    lazy var barWidth: CGFloat = {
        return self.barCollectionView!.frame.size.width/CGFloat(self.plotter!.dataSet.count)
    }()

    public func draw(_ dataSet: [Plottable]) {
        plotter = Plotter(dataSet)
        print((plotter?.maxValue)!)
        print((plotter?.minValue)!)
        print((plotter?.spread)!)
        self.singleBarHeight = self.frame.size.height/CGFloat(plotter!.spread.rawValue())

        self.scaleView = ScaleView(frame: self.frame)

        // TODO: - this is temporary values
        let values: [String] = ["0", "1", "2", "3", "4", "5", "6"]
        scaleView.draw(values, singleBarHeight: singleBarHeight)
        self.addSubview(self.scaleView)
        self.layoutSubviews()
        self.drawCollectionView()
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        guard plotter?.dataSet != nil else {
            self.addPlaceholderForStoryboardPreview()
            return
        }
        self.placeholderImageView?.image = UIImage()
        self.placeholderImageView?.removeFromSuperview()
    }

    private func addPlaceholderForStoryboardPreview() {
        placeholderImageView = UIImageView(frame: self.bounds)
        placeholderImageView?.image = PlotKitCompanion.image("barGraphPlaceholder")
        placeholderImageView?.contentMode = .scaleAspectFit
        self.addSubview(placeholderImageView!)
    }

    private func drawCollectionView() {
        barCollectionView = UICollectionView(frame: self.frame, collectionViewLayout: UICollectionViewFlowLayout())
        let layout: UICollectionViewFlowLayout = (self.barCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout)!
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        barCollectionView?.delegate = self
        barCollectionView?.dataSource = self
        barCollectionView?.register(SingleBarCellCollectionViewCell.self, forCellWithReuseIdentifier: "SingleBarCellCollectionViewCell")
        self.addSubview(self.barCollectionView!)
        self.barCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        self.barCollectionView?.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.barCollectionView?.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.barCollectionView?.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        self.barCollectionView?.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.barCollectionView?.backgroundColor = UIColor.clear
        self.barCollectionView?.reloadData()
    }

}

// MARK: - UICollectionViewDelegate
extension BarGraphView: UICollectionViewDelegate {

}

// MARK: - UICollectionViewDataSource
extension BarGraphView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.plotter!.dataSet.count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SingleBarCellCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: SingleBarCellCollectionViewCell.cellIdentifier, for: indexPath as IndexPath) as? SingleBarCellCollectionViewCell)!
        cell.offset = self.singleBarHeight*CGFloat(plotter!.dataSet[indexPath.row].yValue().rawValue())
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension BarGraphView: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.barWidth, height: collectionView.frame.size.height)
    }
}
