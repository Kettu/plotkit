//
//  ScaleView.swift
//  PlotKit
//
//  Created by Jakub Mazur on 20/08/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import UIKit

internal class ScaleView: UIView {

    private(set) var singleBarHeight: CGFloat = 0
    private(set) var values: [String]!

    internal func draw(_ values: [String], singleBarHeight: CGFloat) {
        self.singleBarHeight = singleBarHeight
        self.values = values
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = UIColor.clear
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: (self.superview?.topAnchor)!).isActive = true
        self.bottomAnchor.constraint(equalTo: (self.superview?.bottomAnchor)!).isActive = true
        self.leftAnchor.constraint(equalTo: (self.superview?.leftAnchor)!).isActive = true
        self.rightAnchor.constraint(equalTo: (self.superview?.rightAnchor)!).isActive = true
        self.drawDividers()
    }

    private func drawDividers() {
        let dividerHeight: CGFloat = 1
        var nextBarOffset: CGFloat = 0
        for value in values {
            let view: UIView = UIView()
            self.addSubview(view)
            view.backgroundColor = UIColor.red
            view.translatesAutoresizingMaskIntoConstraints = false
            view.heightAnchor.constraint(equalToConstant: dividerHeight).isActive = true
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -nextBarOffset+(dividerHeight/2)).isActive = true
            view.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
            view.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
            self.addValueLabel(value, view: view)
            nextBarOffset += singleBarHeight
        }
    }

    private func addValueLabel(_ value: String, view: UIView) {
        let label: UILabel = UILabel()
        label.text = value
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        label.widthAnchor.constraint(equalToConstant: 20).isActive = true
    }

}
