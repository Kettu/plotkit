//
//  PlotKit.h
//  PlotKit
//
//  Created by Jakub Mazur on 22/07/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PlotKit.
FOUNDATION_EXPORT double PlotKitVersionNumber;

//! Project version string for PlotKit.
FOUNDATION_EXPORT const unsigned char PlotKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PlotKit/PublicHeader.h>


