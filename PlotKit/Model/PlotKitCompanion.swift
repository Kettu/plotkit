//
//  PlotKitCompanion.swift
//  PlotKit
//
//  Created by Jakub Mazur on 22/07/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import UIKit

class PlotKitCompanion: NSObject {

    // MARK: Bundles

     private func sdkBundle() -> Bundle {
        return Bundle(for: type(of: self))
    }

    internal class func errorLog(_ errorDsc: String) {
        print(" 📊 plotKit: " + errorDsc)
    }

    class func image(_ imageName: String) -> UIImage? {
        return UIImage(named: imageName, in: PlotKitCompanion().sdkBundle(), compatibleWith: nil)
    }
}
