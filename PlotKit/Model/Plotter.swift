//
//  Plotter.swift
//  PlotKit
//
//  Created by Jakub Mazur on 23/07/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import UIKit

class Plotter: NSObject {

    private(set) var dataSet: [Plottable] = [Plottable]()
    init(_ dataSet: [Plottable]) {
        self.dataSet = dataSet
    }

    lazy internal var spread: YTypeValue = {
        let dataType = type(of: self.maxValue)
        return dataType.sum(args: self.maxValue.absolute(), self.minValue.absolute())
    }()

    lazy internal var maxValue: YTypeValue = {
        let yValues = Plotter.yValues(self.dataSet)
        let dataType = type(of: yValues[0])
        return dataType.max(yValues)
    }()
    lazy internal var minValue: YTypeValue = {
        let yValues = Plotter.yValues(self.dataSet)
        let dataType = type(of: yValues[0])
        return dataType.min(yValues)
    }()

    internal static func yValues(_ dataSet: [Plottable]) -> [YTypeValue] {
        var yValues: [YTypeValue] = [YTypeValue]()
        for data in dataSet { yValues.append(data.yValue()) }
        return yValues
    }
}
