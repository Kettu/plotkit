//
//  XType.swift
//  PlotKit
//
//  Created by Jakub Mazur on 22/07/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import Foundation

public protocol XTypeValue {

}

extension Date: XTypeValue { }
extension String: XTypeValue { }
