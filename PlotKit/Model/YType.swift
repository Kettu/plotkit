//
//  YType.swift
//  PlotKit
//
//  Created by Jakub Mazur on 22/07/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import Foundation

public protocol YTypeValue {
    static func max(_ dataSet: [YTypeValue]) -> YTypeValue
    static func min(_ dataSet: [YTypeValue]) -> YTypeValue
    static func sum(args: YTypeValue...) -> YTypeValue
    func absolute() -> YTypeValue
    func rawValue() -> Double
}

// MARK: - Use this generic function for simplify extensions for simple types.
/*extension YTypeValue where Self: Equatable {
    public static func sum(args: YTypeValue...) -> YTypeValue /*where T:Comparable */ {
        let sum = args.reduce(0, {$0.rawValue() + $1.rawValue()})
        return sum
    }
}*/

/*extension YTypeValue where Self: Comparable {
    public static func maximum<T>(_ dataSet: [T]) -> T where T: Comparable {
        return dataSet.min() ?? (0 as? T)!
    }
}*/

extension Int: YTypeValue {
    public static func min(_ dataSet: [YTypeValue]) -> YTypeValue { return (dataSet as? [Int])?.min() ?? 0 }
    public static func max(_ dataSet: [YTypeValue]) -> YTypeValue { return (dataSet as? [Int])?.max() ?? 0 }
    public func absolute() -> YTypeValue { return abs(self) }
    public func rawValue() -> Double { return Double(self) }
    public static func sum(args: YTypeValue...) -> YTypeValue { return (args as? [Int])?.reduce(0, { $0 + $1 }) ?? 0 }
 }

extension Double: YTypeValue {
    public static func min(_ dataSet: [YTypeValue]) -> YTypeValue { return (dataSet as? [Double])?.min() ?? 0 }
    public static func max(_ dataSet: [YTypeValue]) -> YTypeValue { return (dataSet as? [Double])?.max() ?? 0 }
    public func absolute() -> YTypeValue { return fabs(self) }
    public func rawValue() -> Double { return self }
    public static func sum(args: YTypeValue...) -> YTypeValue { return (args as? [Double])?.reduce(0, { $0 + $1 }) ?? 0 }
 }
