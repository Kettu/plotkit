//
//  ViewController.swift
//  PlotGearDemo
//
//  Created by Jakub Mazur on 15/04/2017.
//  Copyright © 2017 Kettu Jakub Mazur. All rights reserved.
//

import UIKit
import PlotKit

class ViewController: UIViewController {

    @IBOutlet weak var barGraphView: BarGraphView!

    override func viewDidLoad() {
        super.viewDidLoad()
        SessionManager.fixtures(teamId: 64, shouldFakeData: false) { (fixtures, _) in
            DispatchQueue.main.sync {
                self.barGraphView.draw(fixtures!)
            }
        }
    }
}
