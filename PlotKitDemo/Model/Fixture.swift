//
//  Fixture.swift
//  PlotGearDemo
//
//  Created by Jakub Mazur on 17/04/2017.
//  Copyright © 2017 Kettu Jakub Mazur. All rights reserved.
//

import Foundation
import PlotKit

enum MatchStatus: String {
    case finished   = "FINISHED"
    case timed      = "TIMED"
    case scheduled  = "SCHEDULED"
    case postponed  = "POSTPONED"
    case canceled   = "CANCELED"
    case inPlay     = "IN_PLAY"
    case unknown    = "UNKNOWN"
}

enum MatchGround: String {
    case home
    case away
}

struct Fixture {
    var teamName: String?
    var opponentTeamName: String?
    var date: Date?
    var matchDay: Int?
    var goalsFor: Int?
    var goalsAgainst: Int?
    var status: MatchStatus?
    var ground: MatchGround?
    var goalDiff: Int?

    static func isGameOnHomeGround(_ awayName: String, _ homeName: String, _ requestedTeamName: String) -> Bool {
        if awayName == requestedTeamName {
            return false
        } else if homeName == requestedTeamName {
            return true
        }
        return false
    }

    static func withResponse(_ response: [String: Any], team: Team) -> Fixture {
        var fixture: Fixture = Fixture()
        let awayTeamName = (response["awayTeamName"] as? String) ?? ""
        let homeTeamName = (response["homeTeamName"] as? String) ?? ""
        let results: [String: Any] = (response["result"] as? [String: Any])!

        let isHome = isGameOnHomeGround(awayTeamName, homeTeamName, team.name ?? "")
        if isHome == true {
            fixture.teamName = homeTeamName
            fixture.opponentTeamName = awayTeamName
            fixture.goalsFor = results["goalsHomeTeam"] as? Int
            fixture.goalsAgainst = results["goalsAwayTeam"] as? Int
            fixture.ground = .home
        } else {
            fixture.teamName = awayTeamName
            fixture.opponentTeamName = homeTeamName
            fixture.goalsFor = results["goalsAwayTeam"] as? Int
            fixture.goalsAgainst = results["goalsHomeTeam"] as? Int
            fixture.ground = .away
        }
        if let date = ISO8601DateFormatter().date(from: (response["date"] as? String)!) {
            fixture.date = date
        }
        fixture.matchDay = response["matchday"] as? Int
        if let status = MatchStatus(rawValue: (response["status"] as? String)!) {
            fixture.status = status
        }
        fixture.goalDiff = (fixture.goalsFor ?? 0) - (fixture.goalsAgainst ?? 0)
        return fixture
    }
}

extension Fixture: Plottable {
    func yValue() -> YTypeValue {
        return goalsFor!
    }

    func xValue() -> XTypeValue {
        return date!
    }
}
