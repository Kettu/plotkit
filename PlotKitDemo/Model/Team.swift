//
//  Team.swift
//  PlotGearDemo
//
//  Created by Jakub Mazur on 24/05/2017.
//  Copyright © 2017 Kettu Jakub Mazur. All rights reserved.
//

import Foundation

struct Team {
    var code: String?
    var crestURL: String?
    var name: String?
    var shortName: String?

    static func withResponse(_ response: [String: Any]) -> Team {
        var team = Team()
        team.code = response["code"] as? String
        team.crestURL = response["crestURL"] as? String
        team.name = response["name"] as? String
        team.shortName = response["shortName"] as? String
        return team
    }

}
