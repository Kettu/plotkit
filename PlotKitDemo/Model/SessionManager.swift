//
//  SessionManager.swift
//  PlotGearDemo
//
//  Created by Jakub Mazur on 17/04/2017.
//  Copyright © 2017 Kettu Jakub Mazur. All rights reserved.
//

import UIKit

class SessionManager: NSObject {

    internal class func teamDetail(teamId: Int, completionClosure:@escaping(_ team: Team?, _ error: Error?) -> Void) {
        let todoEndpoint: String = "https://api.football-data.org/v1/teams/\(teamId)"
        self.connect(todoEndpoint) { (data, _, error) in
            guard error == nil else {
                completionClosure(nil, error)
                return
            }
            let dResponse: [String: Any]? = SessionManager.deserializeObject(data!) as? [String: Any]
            let team = Team.withResponse(dResponse ?? [:])
            completionClosure(team, error)
        }

    }

    internal class func fixtures(teamId: Int, shouldFakeData: Bool,
                                 completionClosure:@escaping(_ fixtures: [Fixture]?, _ error: Error?) -> Void) {
        teamDetail(teamId: teamId) { (team, error) in
            //TODO: Back results in internet access
            if shouldFakeData == true {
                var fixtures: [Fixture] = [Fixture]()
                let dResponse = SessionManager.readDummyInsteadOfReal(Data())
                let arrayFixtures: [[String: Any]] = (dResponse?["fixtures"] as? [[String: Any]]) ?? [[:]]
                for fixturePlain in arrayFixtures {
                    let fixture = Fixture.withResponse(fixturePlain, team: Team())
                    fixtures.append(fixture)
                }
                completionClosure(fixtures, error)
            } else {
                guard error == nil else {
                    completionClosure(nil, error)
                    return
                }
                let todoEndpoint: String = "https://api.football-data.org/v1/teams/\(teamId)/fixtures"
                guard let request = self.createURLRequest(todoEndpoint) else { return }

                let task = URLSession(configuration: urlSessionConfiguration()).dataTask(with: request) { data, _, error in
                    guard error == nil else {
                        completionClosure(nil, error)
                        return
                    }
                    var fixtures: [Fixture] = [Fixture]()
                    let dResponse = SessionManager.readDummyInsteadOfReal(data!)
                    let arrayFixtures: [[String: Any]] = (dResponse?["fixtures"] as? [[String: Any]]) ?? [[:]]
                    for fixturePlain in arrayFixtures {
                        let fixture = Fixture.withResponse(fixturePlain, team: team!)
                        fixtures.append(fixture)
                    }
                    completionClosure(fixtures, error)
                }
                task.resume()
            }
        }
    }

    class func readDummyInsteadOfReal(_ realData: Data) -> [String: Any]? {
        let path = Bundle.main.url(forResource: "LFCFixtures", withExtension: ".json")
        do {
            let dummyData = try Data.init(contentsOf: path!)
            return SessionManager.deserializeObject(dummyData) as? [String: Any]
        } catch {
            print("Error reading fake data")
        }
        return SessionManager.deserializeObject(realData) as? [String: Any]
    }

    internal class func deserializeObject(_ data: Data) -> Any? {
        do {
            let object: [String: Any]? = try (JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any])
            return object
        } catch {
            return nil
        }
    }

    internal class func connect(_ urlString: String, completionClosure:@escaping( _ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        guard let request = self.createURLRequest(urlString) else {
            completionClosure(nil, nil, NSError(domain: "pl.kettu.plotGearDemo", code: 105, userInfo: nil))
            return
        }
        let task = URLSession(configuration: urlSessionConfiguration()).dataTask(with: request) { data, response, error in
            completionClosure(data, response, error)
        }
        task.resume()
    }

    internal class func createURLRequest(_ stringURL: String) -> URLRequest? {
        guard let url = URL(string: stringURL) else {
            print("Error: cannot create URL")
            return nil
        }
        return URLRequest(url: url)
    }

    internal class func urlSessionConfiguration() -> URLSessionConfiguration {
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["X-Auth-Token": "4b57b28eec4c464fa30cb64a67878aa5"]
        return config
    }
}
