//
//  AppDelegate.swift
//  PlotKitDemo
//
//  Created by Jakub Mazur on 22/07/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}
