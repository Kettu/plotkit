//
//  BarGraphView.swift
//  PlotKitTests
//
//  Created by Jakub Mazur on 16/10/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import XCTest
@testable import PlotKit

class BarGraphViewTests: XCTestCase {

    let view: BarGraphView = BarGraphView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 100, height: 100)))
    let dataSet: [IntDataSet] = IntDataSet.set()

    func testLayoutSubview() {
        view.layoutSubviews()
        XCTAssertTrue(view.subviews[0] is UIImageView, "📉 should be placeholder image")
        view.draw(dataSet)
        view.layoutSubviews()
        XCTAssertFalse(view.subviews[0] is UIImageView, "📉 shouldn't be placeholder image anymore")
        let filtered = view.subviews.filter { $0 is UICollectionView }
        XCTAssertTrue(filtered.count == 1, "📉 Collection view should appear and should be only one")
    }

    func testDraw() {
        view.draw(dataSet)
        XCTAssertTrue(view.plotter?.dataSet.count == dataSet.count, "📉 Should be equal")
    }
}

// MARK: - UICollectionViewDelegate
extension BarGraphViewTests {

}

// MARK: - UICollectionViewDataSource
extension BarGraphViewTests {
    func testCollectionViewNumberOfItemsInSection() {
        view.draw(dataSet)
        let items = self.view.collectionView(self.view.barCollectionView!, numberOfItemsInSection: 0)
        XCTAssertTrue(items == dataSet.count, "📉 Should be equal")
    }

    func testCollectionViewCellForItemAt() {
        view.draw(dataSet)
        let cell = self.view.collectionView(self.view.barCollectionView!, cellForItemAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is SingleBarCellCollectionViewCell, "📉 Cell should be always single view class")
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension BarGraphViewTests {
    func testCollectionViewLayoutSizeForItem() {
        view.draw(dataSet)
        let size = self.view.collectionView(self.view.barCollectionView!, layout: self.view.barCollectionView!.collectionViewLayout, sizeForItemAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(size.width > 0, "📉 width shouldn't be 0 in any circumstances")
        XCTAssertEqual(size.height, self.view.barCollectionView!.frame.size.height, "📉 height should be equal entire collection view height")
    }
}
