//
//  PlotterTests.swift
//  PlotKit
//
//  Created by Jakub Mazur on 12/08/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import XCTest
@testable import PlotKit

class PlotterTests: XCTestCase {

    func testInitInt() {
        let plotter: Plotter = Plotter(IntDataSet.set())
        XCTAssertEqual(plotter.dataSet.count, IntDataSet.plainValues.count, "📉 Should be equal after init")
    }

    func testyValues() {
        let yTypeValues = Plotter.yValues(IntDataSet.set())
        XCTAssertEqual(yTypeValues.count, IntDataSet.plainValues.count, "📉 Somethind went wrong with parsing. Should be equal")
    }

}
