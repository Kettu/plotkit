//
//  IntDataSet.swift
//  PlotKit
//
//  Created by Jakub Mazur on 12/08/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import UIKit
import PlotKit

class IntDataSet: NSObject {
    var exyValue: Int?
    var exxValue: Date?

    static let plainValues: [Int] = [2, 7, 36, 0, 0, 1, -4, 90, 1, 3]

    static func set() -> [IntDataSet] {
        var returnValue: [IntDataSet] = [IntDataSet]()
        for value in IntDataSet.plainValues {
            let obj = IntDataSet()
            obj.exyValue = value
            obj.exxValue = Date()
            returnValue.append(obj)
        }
        return returnValue
    }
}

extension IntDataSet: Plottable {
    func yValue() -> YTypeValue {
        return self.exyValue!
    }

    func xValue() -> XTypeValue {
        return self.exxValue!
    }
}
