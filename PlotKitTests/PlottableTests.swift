//
//  PlottableTests.swift
//  PlotKitTests
//
//  Created by Jakub Mazur on 18/10/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import XCTest

class PlottableTests: XCTestCase {
    func testPlottable() {
        let set: [IntDataSet] = IntDataSet.set()
        for obj in set {
            _ = obj.xValue()
            _ = obj.yValue()
        }
    }
}
