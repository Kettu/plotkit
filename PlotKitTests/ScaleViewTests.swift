//
//  ScaleViewTests.swift
//  PlotKitTests
//
//  Created by Jakub Mazur on 18/10/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import XCTest
@testable import PlotKit

class ScaleViewTests: XCTestCase {

    let scaleView: ScaleView = ScaleView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 100)))

    func testLayoutSubviews() {
        scaleView.layoutSubviews()
        XCTAssertTrue(scaleView.backgroundColor == UIColor.clear, "📉 scale should always have transparent background")
    }

    func testDrawValues() {
        let container: UIView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 100)))
        container.addSubview(scaleView)
        scaleView.draw(["1", "2", "3", "4"], singleBarHeight: 10)
        scaleView.draw(scaleView.frame)
    }

}
