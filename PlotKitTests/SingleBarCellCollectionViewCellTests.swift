//
//  SingleBarCellCollectionViewCell.swift
//  PlotKitTests
//
//  Created by Jakub Mazur on 18/10/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import XCTest
@testable import PlotKit

class SingleBarCellCollectionViewCellTests: XCTestCase {

    let cell: SingleBarCellCollectionViewCell = SingleBarCellCollectionViewCell()

    func testPrepareForReuse() {
        cell.prepareForReuse()
    }

}
