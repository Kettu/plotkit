//
//  YTypeTests.swift
//  PlotKitTests
//
//  Created by Jakub Mazur on 14/10/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import XCTest
@testable import PlotKit

class YTypeTests: XCTestCase {
    let exampleIntSet: [Int] = [-1, 0, -1, 5, 78, -97, 6]
    let exampleDoubleSet: [Double] = [-5, 32, 32, 9, 7.5, 4.22, -7.9, -7.91, 8.5]
}

// MARK: - Int
extension YTypeTests {
    func testIntMin() {
        XCTAssertEqual(Int.min(exampleIntSet) as? Int, -97, "📉 Check your data set or there is something wrong with min function")
    }

    func testIntMax() {
        XCTAssertEqual(Int.max(exampleIntSet) as? Int, 78, "📉 Check your data set or there is something wrong with max function")
    }

    func testIntAbsolute() {
        let plusInt: Int = 4
        let zeroInt: Int = 0
        let minusInt: Int = -5
        XCTAssertEqual(plusInt, plusInt.absolute() as? Int, "📉 For absolute values things greater than 0 should be the same")
        XCTAssertEqual(zeroInt, zeroInt.absolute() as? Int, "📉 For absolute values 0 should stay 0")
        XCTAssertEqual(minusInt * -1, minusInt.absolute() as? Int, "📉 For absolute values things below 0 should change sign")
    }

    func testIntRawValue() {
        let exampleInt: Int = 4
        let expectedResult: Double = Double(exampleInt)
        XCTAssertEqual(exampleInt.rawValue(), expectedResult, "📉 Expected result should be equal simple transfer")
    }

    func testIntSum() {
        let sum = Int.sum(args: 4, 5, -1, 0, 9, 12, -7)
        XCTAssertEqual(sum as? Int, 22, "📉 Double check if this example numbers above really equal 22 but if so, there is issue with sum method")
    }
}

// MARK: - Double
extension YTypeTests {
    func testDoubleMin() {
        let minimum = Double.min(exampleDoubleSet)
        XCTAssertEqual(minimum as? Double, -7.91, "📉 Check your data set or there is something wrong with min function \(minimum)")
    }

    func testDoubleMax() {
        let maximum = Double.max(exampleDoubleSet)
        XCTAssertEqual(maximum as? Double, 32, "📉 Check your data set or there is something wrong with max function \(maximum)")
    }

    func testDoubleAbsolute() {
        let plusInt: Double = 4.6
        let zeroInt: Double = 0
        let minusInt: Double = -5.1
        XCTAssertEqual(plusInt, plusInt.absolute() as? Double, "📉 For absolute values things greater than 0 should be the same")
        XCTAssertEqual(zeroInt, zeroInt.absolute() as? Double, "📉 For absolute values 0 should stay 0")
        XCTAssertEqual(minusInt * -1, minusInt.absolute() as? Double, "📉 For absolute values things below 0 should change sign")
    }

    func testDoubleRawValue() {
        let exampleInt: Double = 4
        let expectedResult: Double = Double(exampleInt)
        XCTAssertEqual(exampleInt.rawValue(), expectedResult, "📉 Expected result should be equal simple transfer")
    }

    func testDoubleSum() {
        let sum = Double.sum(args: 4.2, 5.1, -1.05, 0.0, 9.9, 12.1, -7.4, 8.0, 2.0)
        XCTAssertEqual(sum as? Double, 32.85, "📉 Double check if this example numbers above really equal 22 but if so, there is issue with sum method")
    }
}
