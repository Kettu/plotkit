//
//  PlotKitCompanionTests.swift
//  PlotKitTests
//
//  Created by Jakub Mazur on 18/10/2017.
//  Copyright © 2017 Jakub Mazur. All rights reserved.
//

import XCTest
@testable import PlotKit

class PlotKitCompanionTests: XCTestCase {

    func testImage() {
        let image = PlotKitCompanion.image("barGraphPlaceholder")
        XCTAssertNotNil(image, "📉 image should exist, in case of this error please check if this exist in Assets, and the check this macro method")
    }

    func testErrorLog() {
        PlotKitCompanion.errorLog("Some string")
    }

}
